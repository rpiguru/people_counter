import signal
import MySQLdb as mysqlinst
import sqlite3 as lite
import logging
import platform
import os, sys
import time
import xml.etree.ElementTree


def kill_previous_proc(process_name, count):
    pid_list = []

    cmd_result = os.popen("ps ax | grep " + process_name + " | grep -v grep")

    for line in cmd_result:
        fields = line.split()
        pid_list.append(fields[0])

    # if app is already running, the list's size is greater than 2
    if len(pid_list) > 1:
        if count > 1:
            for i in range(len(pid_list) - 1):
                os.kill(int(pid_list[i]), signal.SIGKILL)	# kill all process
                logging.info("Killing previous process... %s" % pid_list[i])
        else:
            os.kill(int(pid_list[0]), signal.SIGKILL)	# kill 1st process(old)
            logging.info("Killing previous process... %s" % pid_list[0])

    return True


def get_captured_data():
    conn = None
    try:
        print "connecting to the local database..."
        conn = lite.connect(local_db_file)
        os.system("sudo chmod 777 " + local_db_file)

        curs = conn.cursor()
        sql = "CREATE TABLE IF NOT EXISTS counters (id INTEGER PRIMARY KEY AUTOINCREMENT," \
              " count NUMERIC, direction TEXT, time TEXT);"
        curs.execute(sql)
        conn.commit()

        curs.execute("SELECT * FROM counters")
        rows = curs.fetchall()
        conn.close()
        data_list = []
        for i in range(len(rows)):
            data_list.append(list(rows[i]))
        return data_list

    except lite.Error as e:
        print("Error %s:" % e.args[0])
        logging.error(" Failed to get captured data")
    finally:
        if conn:
            conn.close()


def clear_local_db(row_count):
    con_dev = None
    try:
        con_dev = lite.connect(local_db_file)
        curs_tmp = con_dev.cursor()
        sql = "DELETE FROM counters;"
        curs_tmp.execute(sql)
        con_dev.commit()
        con_dev.close()
        logging.info("%s rows has been deleted..." % str(row_count))
        return True

    except lite.Error as e:
        print("Error %s:" % e.args[0])
        logging.info("Error %s:" % e.args[0])
        logging.error("Deleting DB failed...")
        return False
    finally:
        if con_dev:
            con_dev.close()


def upload_data_to_aws(data):

    server_db = None
    cursor = None
    try:
        # connect to MySQL data base
        print 'connecting to AWS DB...'
        server_db = mysqlinst.connect(host=server_name, user=server_username, passwd=server_password, db=server_db_name)
        print "connected, and now uploading data..."

        # create table if not exists
        cursor = server_db.cursor()
        #    query = "CREATE TABLE IF NOT EXISTS " + table_name + " (id INTEGER PRIMARY KEY AUTO_INCREMENT" \
        #                ", timestamp TEXT" \
        #                ", locationID TEXT" \
        #                ", event NUMERIC" \
        #                ");"
        #    print query

        #    cursor.execute(query)
        #    server_db.commit()

        # get host name
        uname = platform.uname()
        loc_id = uname[1]

        # upload data to AWS server
        for dd in data:
            var_dt = dd[3]
            if dd[2] == 'in':
                var_event = 1
            else:
                var_event = -1

            query = "INSERT INTO " + server_table_name + "(timestamp, locationID, event) VALUES('" \
                    + var_dt + "', '" + loc_id + "', " + str(var_event) + ");"
#            print query

            cursor.execute(query)
            server_db.commit()
        cursor.close()
        server_db.close()
        logging.info("commiting done...")
        print "Data committed"
        return True
    except mysqlinst.Error as error:
        print("Error %s:" % error)
        logging.info("Error : %s" % error)
        return False


def get_param_from_xml(param):
    root = xml.etree.ElementTree.parse(current_path + '/config.xml').getroot()
    tmp = None
    for child_of_root in root:
        if child_of_root.tag == param:
            tmp = child_of_root.text
    return tmp


def count_down(s):
    for i in range(s, 0, -1):
        print i,
        sys.stdout.flush()
        time.sleep(1)
    print

##############################################################################
#                               main script
##############################################################################

# get constant values from config file

current_path = os.path.dirname(os.path.abspath(__file__))
proc_name = get_param_from_xml('ProcName')

local_DB = get_param_from_xml('Local_dbFile')
local_db_file = current_path + "/" + local_DB

server_name = get_param_from_xml('AWS_HOST')
server_db_name = get_param_from_xml('AWS_db_name')
server_table_name = get_param_from_xml('AWS_table_name')
server_username = get_param_from_xml('AWS_user_name')
server_password = get_param_from_xml('AWS_password')

log_file_name = current_path + '/' + get_param_from_xml('Log_FileName')
logging.basicConfig(level=10, filename=log_file_name, format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


# kill previous app/script
kill_previous_proc(proc_name, 1)
kill_previous_proc("pre_kill", 2)

time.sleep(3)

debug = get_param_from_xml('DEBUG')

# iterate uploading
while True:

    ddd = get_captured_data()
    if debug == 1:
        if ddd:
            for d in ddd:
                print d
        else:
            print "No data captured..."
    else:
        if ddd:
            if upload_data_to_aws(ddd):
                clear_local_db(len(ddd))
            else:
                print "Uploading failed, try again later..."
        else:
            print "No data captured..."

    interval = get_param_from_xml('UPLOAD_INTERVAL')

    if debug == 1:
        count_down(int(interval))
    else:
        time.sleep(int(interval))







