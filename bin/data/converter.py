import os
import sys

if __name__ == "__main__":

    if len(sys.argv) < 2:
        print "Please select input file name"
        sys.exit(0)
    else:
        file_name = sys.argv[1]
        cmd = "ffmpeg -y -i " + file_name + " -r 25 -s 320x240 -c:v libx264 -b:v 3M -strict -2 -movflags faststart " \
                                            "converted_" + file_name
        os.system(cmd)




