// * Name: ofApp.h
// * Project: People Counter
// * Author: Wester de Weerdt
// * Creation Date: 04-03-2016

#pragma once

#include "ofMain.h"
#include "ofxCv.h"
#include <stdio.h>
#include "SQLiteCppUtils.h"
#include "Blob.h"
#include "ofxCvPiCam.h"
#include "ofxXmlSettings.h"

using namespace ofxCv;
using namespace cv;

#define USE_CAMERA

//--------------------------------------------------------------
class myLine {
public:
    int width;
    void draw(int x, int y)
    {
        ofLine(x,y,x-width/2,y);
        ofLine(x,y,x+width/2,y);
    }
};

//--------------------------------------------------------------
class ofApp : public ofBaseApp {
public:
    void setup();
    void update();
    void draw();
    void exit();
    void keyPressed(int key);
    
    void setupCV();
    void drawConfig();
    void makeMask();
	void setup_SQLite();
	
	void upload_counter(int cnt_val, string drt);

    ofxCvPiCam cam;
    ofVideoPlayer videoPlayer;
    ofVideoPlayer fingerMovie;

    ofxCv::ContourFinder contourFinder;
    ofxCv::RectTrackerFollower<Blob> tracker;
    cv::BackgroundSubtractorMOG2* pMOG2;
    cv::Mat frame,resizeF,fgMaskMOG2,output;
    cv::Mat mask,maskOutput;
    cv::Mat lightenMat;
    string responseStr;
    string requestStr;
	
    bool counterLatches[30];
    int count;
    int countIn;
    int countOut;
    bool countInLatch;
    bool countOutLatch;
    SQLite::Database *myDB;
	
    void loadConfig();
    ofxXmlSettings XML;
    int _contourThreshold;
    int _threshold;
    int _blur;
    int _minArea;
    int _maxArea;
    int _persistance;
    int _maxDistance;
    int _minBlobSize;
    int _midBlobSize;
    int _maxBlobSize;
    int _lineYPos;
    int _lineWidth;
    int _cameraWidth;
    int _cameraHeight;
    int _history;
    int _MOGThreshold;
    int _lightenAmount;
    int _contrast;
    int _brightness;
    int _log_level;
    int _debug;
    float _speed;
    bool bTrack;

    vector <cv::Point> _maskPoints;

    string _log_file_name;
    string _locationID;
    string _secretKey;
    string _uploadurl;
    string _uploadFileURL;
	string _dbName;
	int _source;
    string _video_file_name;

    std::deque<myLine> lines;
    std::deque<string> actions;
    ofPoint startLine;
    ofPoint endLine;
};


