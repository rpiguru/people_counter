import sys, os
import logging
import sqlite3 as lite

current_path = os.path.dirname(os.path.abspath(__file__))


def show_all_data(db_name):
    conn = None
    file_name = 'counterDB.sqlite'
    try:
        conn = lite.connect(file_name)
        curs = conn.cursor()
        print('getting device list...')
        query = "SELECT * FROM " + db_name + ";"
        print(query)
        curs.execute(query)
        rows = curs.fetchall()
        conn.close()
        print(rows)
        if len(rows) > 0:
            logging.info("Getting serials from DB...")
        return rows
    except lite.Error as e:
        print("Error %s:" % e.args[0])
        logging.error("Getting data from DB failed...")

    finally:
        if conn:
            conn.close()


if __name__ == '__main__':

    param = sys.argv[1]

    show_all_data(param)









